trivy -d fs --format spdx --output trivy-sbom-spdx.sbom .
trivy -d fs --format spdx-json --output trivy-sbom-spdx.json .
trivy -d fs --format cyclonedx --output trivy-sbom-cyclonedx.json .
npm sbom --sbom-format cyclonedx | tee -a npm-sbom-cyclonedx.json
npm sbom --sbom-format spdx | tee -a npm-sbom-spdx.sbom
syft dir:. | tee -a syft.sbom.sbom