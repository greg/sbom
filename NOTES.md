## Notes :notebook:

- https://github.com/aquasecurity/trivy (SBOM generation, optional SBOM vulnerability scanning)
- https://github.com/anchore/syft (SBOM generation, convert)
- https://github.com/CycloneDX/cdxgen (SBOM generation, CycloneDX format)
- https://github.com/oss-review-toolkit/ort (SBOM generation)
- https://github.com/owasp-dep-scan/dep-scan (SBOM generation and vulnerability scanning 2-in-1)
- https://github.com/CycloneDX/cyclonedx-cli 
- https://github.com/cyclonedx/sbom-utility
- https://github.com/microsoft/sbom-tool (generation/validation)
- https://github.com/eBay/sbom-scorecard (SBOM validation/scoring)
- https://github.com/interlynk-io/sbomqs (SBOM validation/scoring)
- https://github.com/anchore/grype (SBOM vulnerability scanning)
- https://github.com/devops-kung-fu/bomber (SBOM vulnerability scanning)
- https://github.com/CycloneDX/cyclonedx-cli (edit, diff, convert, merge SBOMs)
- https://github.com/sigstore/cosign (attestation)
- https://sbombenchmark.dev/ - Quickly evaluate SBOM for quality, compliance and errors